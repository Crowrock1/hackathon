import React from "react";
import './SidebarNotepad.css';

const SidebarNotepad = () => {

    const downloadTxtFile = () => {
        const element = document.createElement("a");
        const file = new Blob([document.getElementById('text').value], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "myFile.txt";
        document.body.appendChild(element);
        element.click();
      }

    return (
        <div id="wrapper">
            <textarea placeholder="Zde můžete zapisovat své poznámky a následně si je můžete stáhnot (.txt)." id="text" name="text"></textarea>  
            <button id="button" onClick={downloadTxtFile}>Stáhnout poznámky</button>         
        </div>
    )
}
export default SidebarNotepad;