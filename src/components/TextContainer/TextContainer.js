import React from 'react';



import './TextContainer.css';

const TextContainer = ({ users }) => (
  <div className="textContainer">
      <h2 class="students-list-heading">Students</h2>
    {
      users
        ? (
          <div>
            <div className="activeContainer">
              <h4>
                {users.map(({name}) => (
                  <div key={name} className="activeItem">
                    <div className="status complete "></div>
                    {name}
                  </div>
                ))}
              </h4>
            </div>
          </div>
        )
        : null
    }
  </div>
);

export default TextContainer;