import React, { useState } from "react";
import { Link } from "react-router-dom";

import "./Join.css"

const Join = () => {
    const [name, setName] = useState("");
    const [room, setRoom] = useState("");
    
    return (
        <div className="join-container">
            <div className="top-text">
                <h2 className="join-container-header">Vytvoření | Připojení</h2>
                <p className="join-container-text">Vytvořte si vlastní chatovací místnost, nebo se připojte do již vygenerované chatovací místnosti.</p> 
            </div>
            <span>Přezdívka</span>
            <div><input className="joinInput" type="text" onChange={(event) => setName(event.target.value)} /></div>
            <span>ID Místnosti</span>
            <div><input className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} /></div>
            <Link onClick={event => (!name || !room) ? event.preventDefault() : null} to={`chat?name=${name}&room=${room}`}>
                <button className="button-submit" type="submit">Vytvořit nebo připojit</button>
            </Link>
        </div>
    )
}

export default Join;